#!/bin/bash
#
#
# Create a local Docker Resgistry
#
# https://hub.docker.com/_/registry

CMD=${1:-"help"}
CMD_ARGS_LEN=${#}

#echo $CMD $CMD_ARGS_LEN


# envvars
if [ -f envvars.sh ]; then
  . envvars.sh
else
  echo "Warning: envvars.sh not found"
fi

# if [ -f docker-ctl.sh ]; then
#   . docker-ctl.sh pass
# else
#   echo "Warning: docker-ctl.sh not found"
# fi

KUBECTL_CMD="microk8s.kubectl"


# Check docker: Linux or Ubuntu snap
DOCKER_CMD=`which docker`
DOCKER_CMD=${DOCKER_CMD:-"/snap/bin/microk8s.docker"}
#echo "Using: "$DOCKER_CMD
if [ -d $DOCKER_CMD ]; then
    echo "Docker is missing: "$DOCKER_CMD
    exit 1
fi


_pushContainer() {
  TARGET_DIR=$1
  CURRENT_DIR=`pwd`
  echo "Push container: $TARGET_DIR"
  cd $TARGET_DIR
  . envvars.docker.sh
  ../docker-ctl.sh  push
  cd $CURRENT_DIR
}

_buildContainer() {
  DOCKERFILE="./$DOCKERFILES_DIR/$DOCKER_TAG_NAME.Dockerfile"
  echo "Building $DOCKERFILE Tag: $DOCKER_TAG_NAME"
  $DOCKER_CMD build \
    --build-arg USER=$USER \
    --build-arg HOME_DIR=$HOME_DIR \
    --build-arg PRESTO_VERSION=$PRESTO_VERSION \
    --build-arg PRESTO_SERVER_ARCHIVE=$PRESTO_SERVER_ARCHIVE \
    --build-arg PRESTO_CLI_ARCHIVE=$PRESTO_CLI_ARCHIVE \
    --build-arg PRESTO_TMP_DIR=$PRESTO_TMP_DIR \
    --build-arg PRESTO_DIR=$PRESTO_DIR \
    --build-arg PRESTO_DATA_DIR=$PRESTO_DATA_DIR \
    -t $DOCKER_TAG_NAME \
    --file $DOCKERFILE .
}

_createContainer() {
  echo "1"
}

_modify_Properties_file() {
  echo "Updating properties $1 to $2 in $3"
  V1=$(printf '%s\n' "$1"      | sed 's/[[\.*^$/]/\\&/g'   )
  NEW_VAL=$(printf '%s\n' "$2" | sed 's/[[\.*^$/]/\\&/g'   )
  MFILE=$3
  sed -i".backup"  "s/.*$V1.*/$V1=$NEW_VAL/" "$MFILE"
}

_starburstDataPrestoConfigureCoordinator() {
  cp $PRESTO_DIR/etc/coordinator.config.properties $PRESTO_DIR/etc/config.properties
  _modify_Properties_file node.id $(uuidgen) $PRESTO_DIR/etc/node.properties
  _modify_Properties_file node.data-dir $PRESTO_DATA_DIR $PRESTO_DIR/etc/node.properties
}

_prestoConfigureCoordinator() {
  cp $PRESTO_DIR/etc/coordinator.config.properties $PRESTO_DIR/etc/config.properties
  _modify_Properties_file node.id $(uuidgen) $PRESTO_DIR/etc/node.properties
  _modify_Properties_file node.data-dir $PRESTO_DATA_DIR $PRESTO_DIR/etc/node.properties
}

_prestoConfigureWorker() {
  cp $PRESTO_DIR/etc/worker.config.properties $PRESTO_DIR/etc/config.properties
  _modify_Properties_file node.id $(uuidgen) $PRESTO_DIR/etc/node.properties
  _modify_Properties_file node.data-dir $PRESTO_DATA_DIR $PRESTO_DIR/etc/node.properties
}

_dockerRun() {
  # Adds ports
  # Adds RW volume on host
  DOCKER_EXTRA_ARGS="$1"
  DOCKER_CONTAINER_ID=${2:-"0"}
  echo "Docker running $DOCKER_TAG_NAME ARGS[$DOCKER_EXTRA_ARGS]"
  $DOCKER_CMD run --rm --detach  \
            --name "$DOCKER_TAG_NAME$DOCKER_CONTAINER_ID" \
            --hostname "$DOCKER_TAG_NAME$DOCKER_CONTAINER_ID" \
            --network $DOCKER_NETWORK_NAME \
            $DOCKER_EXTRA_ARGS \
            -it                \
            $DOCKER_TAG_NAME

  # --volume /tmp/dock-$DOCKER_TAG_NAME:/$DOCKER_TAG_NAME:rw \
}

_mysqlStartDB() {
  TMP_LOG="${CONTAINER_CONFIG_DIR}/log.txt"
  DB_HOSTNAME=$(hostname)

  echo "MySQL configuring ${CONTAINER_CONFIG_DIR}" >> $TMP_LOG

  # MySQl config file
  cp /etc/mysql/conf.d/mysql.cnf mysql.cnf.1
  cp ${CONTAINER_CONFIG_DIR}/mysql-config/mysql.cnf /etc/mysql/conf.d/

  # Configure and start mysql
  /entrypoint.sh mysqld & >> $TMP_LOG

  # Wait for DB to Start
  while ! mysqladmin ping -h"$DB_HOSTNAME" -pwelcome1 --silent; do
      echo "Waiting for DB to start $(date)" >> $TMP_LOG
      sleep 2
  done
  echo "MySql DB Ready $(date)" >> $TMP_LOG

  # Configure DB, Create databases
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/mysql-config/create-db.sql >> $TMP_LOG

  # Create iris
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/iris-data-set/iris-data-set.sql >> $TMP_LOG
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/mysql-config/query-iris.sql >> $TMP_LOG

  # Create baseball
  gzip -d ${CONTAINER_CONFIG_DIR}/baseball/lahman-mysql-dump.sql.gz
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/baseball/lahman-mysql-dump.sql >> $TMP_LOG
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/mysql-config/query-baseball.sql >> $TMP_LOG

  # Create Learning SQL DB
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/LearningSQLExample.sql orielly >> $TMP_LOG

  # Create the Sakila database
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/sakila-db/sakila-schema.sql >> $TMP_LOG
  mysql -hlocalhost -uroot -pwelcome1 < ${CONTAINER_CONFIG_DIR}/sakila-db/sakila-data.sql >> $TMP_LOG
  mysql -hlocalhost -uroot -pwelcome1 -e "select 'sakila' as DB, count(*) from sakila.actor;" sakila >> $TMP_LOG

  echo "MySQL started" >> $TMP_LOG
}

_postgresStartDB() {
  TMP_LOG="log.txt"
  ./docker-entrypoint.sh postgres &
  # Wait until database is ready
  while ! pg_isready; do
      echo "Waiting for Postgres Database: $(date)" >> $TMP_LOG
      sleep 2
  done
  echo "Postgres DB ready: $(date)" >> $TMP_LOG
  DB_NAME="db1"
  psql -U postgres -c "ALTER USER postgres PASSWORD 'welcome1';"
  psql -U postgres -c "CREATE DATABASE $DB_NAME;"
  psql -U postgres -c "CREATE SCHEMA airline;" $DB_NAME
  #psql -h localhost -U postgres -f postgres-config/create-db.sql
  psql -h localhost -U postgres -f ${CONTAINER_CONFIG_DIR}/flight-data-set/airport.sql $DB_NAME >> $TMP_LOG
  psql -h localhost -U postgres -f ${CONTAINER_CONFIG_DIR}/flight-data-set/carrier.sql $DB_NAME >> $TMP_LOG
  psql -h localhost -U postgres -f ${CONTAINER_CONFIG_DIR}/postgres-config/query-carrier.sql $DB_NAME >> $TMP_LOG
  psql -h localhost -U postgres -f ${CONTAINER_CONFIG_DIR}/postgres-config/query-airport.sql $DB_NAME >> $TMP_LOG

  echo "PostgresSQL started" >> $TMP_LOG
}

_holdContainerOpen() {
  count=999999
  interval=60
  for i in $(seq $count )
  do
    echo "$i `date`" >> /tmp/container.log
    sleep $interval;
  done;
}

_mySqlMods() {
  # Mod for mysql for airline data
  sed -i".backup" 's/DEFAULT NULL::bpchar,/NOT NULL,/g' downloads/flight-data-set/airport.sql
  sed -i".backup" 's/ character(/ varchar(/g' downloads/flight-data-set/airport.sql
  sed -i".backup" 's/ character varying(/ varchar(/g' downloads/flight-data-set/airport.sql
  sed -i".backup" 's/long double precision,/lon double precision,/g' downloads/flight-data-set/airport.sql
  sed -i".backup" 's/lat, long, tz/lat, lon, tz/g' downloads/flight-data-set/airport.sql

  sed -i".backup" 's/ character varying(/ varchar(/g' downloads/flight-data-set/carrier.sql
  sed -i".backup" "s/DEFAULT ''::character varying NOT NULL/NOT NULL/g" downloads/flight-data-set/carrier.sql
}

ALL_BUILD_LIST=("ubuntu-base" "presto-base" "presto-coordinator" "presto-worker" "mysql" "postgres" "starburstdata-presto-coordinator")
ALL_RUN_LIST=("presto-coordinator" "presto-worker" "mysql")

case "$CMD" in
  test)
    echo "Test"
    ;;
  build) # Expects Argument APP_ID
    DOCKER_TAG_NAME=${2:-"DOCKER_TAG_MISSING"}

    _buildContainer $DOCKER_TAG_NAME
    ;;
  bash)
    DOCKER_TAG_NAME=${2:-"DOCKER TAG MISSING"}
    ./docker-ctl.sh bash $DOCKER_TAG_NAME
    ;;
  stop)
    DOCKER_TAG_NAME=${2:-"DOCKER TAG MISSING"}
    ./docker-ctl.sh stop $DOCKER_TAG_NAME
    ;;
  run)
    DOCKER_TAG_NAME=${2:-"DOCKER TAG MISSING"}
    ID=${3:-"0"}
    EXTRA_ARGS=""
    if [ $DOCKER_TAG_NAME == "presto-coordinator" ]; then
        EXTRA_ARGS="-p 127.0.0.1:8080:8080"
    elif [ $DOCKER_TAG_NAME == "starburstdata-presto-coordinator" ]; then
        EXTRA_ARGS="-p 127.0.0.1:8080:8080"
    elif [ $DOCKER_TAG_NAME$ID == "mysql0" ]; then
        EXTRA_ARGS="-p 127.0.0.1:3306:3306"
    fi
    _dockerRun "$EXTRA_ARGS" $ID $DOCKER_TAG_NAME
  ;;
  mysql)
    mysql -hlocalhost -uroot -pwelcome1
  ;;
  docker-create-net)
    docker network create -d bridge $DOCKER_NETWORK_NAME
  ;;
  mysql-start-db)
    _mysqlStartDB
    _holdContainerOpen
  ;;
  postgres-start-db)
    _postgresStartDB
    _holdContainerOpen
  ;;
  start-all)
   $DOCKER_CMD network create -d bridge $DOCKER_NETWORK_NAME 2> /dev/null
  ./ctl.sh docker-run-mysql 0
  ./ctl.sh docker-run-mysql 1
  ./ctl.sh docker-run-coordinator
  ./ctl.sh docker-run-worker 0
  ./ctl.sh docker-run-worker 1
  ;;
  ubuntu-update)
    sudo apt-get update
    DEBIAN_FRONTEND=noninteractive sudo apt-get -yqq upgrade
    DEBIAN_FRONTEND=noninteractive sudo apt-get -yqq install zip jq
    ;;
  configure)
    # All downloads go into downloads dir.
    mkdir -p downloads

    # Presto Archives
    curl https://repo1.maven.org/maven2/io/prestosql/presto-server/339/presto-server-339.tar.gz   -o downloads/presto-server-339.tar.gz
    curl https://repo1.maven.org/maven2/io/prestosql/presto-cli/339/presto-cli-339-executable.jar -o downloads/presto-cli-339-executable.jar
    chmod +x downloads/presto-cli-339-executable.jar
    ln -s downloads/presto-cli-339-executable.jar presto

    # Sean Lahmans Baseball DB - http://www.seanlahman.com/
    curl https://raw.githubusercontent.com/WebucatorTraining/lahman-baseball-mysql/master/lahman-mysql-dump.sql -o downloads/lahman-mysql-dump.sql
    sed -i".backup" 's/lahmansbaseballdb/baseball/g'             downloads/lahman-mysql-dump.sql
    sed -i".backup" 's/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/g'  downloads/lahman-mysql-dump.sql
    gzip downloads/lahman-mysql-dump.sql

    # Presto Definitaive Guide Data
    git -C downloads clone https://github.com/prestosql/presto-the-definitive-guide
    sed -i".backup" 's/USE memory.default;/USE irisdb;/g' downloads/presto-the-definitive-guide/iris-data-set/iris-data-set.sql

    # Orielly Learning SQL
    curl https://resources.oreilly.com/examples/9780596007270/raw/master/LearningSQLExample.sql -o downloads/LearningSQLExample.sql

    # MySQl Sakila Database
    curl https://downloads.mysql.com/docs/sakila-db.tar.gz -o sakila-db.tar.gz

    # CSV Data
    curl http://s3.amazonaws.com/presto-example/v2/example-metadata.json  -o csv-data/example-metadata.json
    curl http://s3.amazonaws.com/presto-example/v2/numbers-1.csv          -o csv-data/numbers-1.csv
    curl http://s3.amazonaws.com/presto-example/v2/numbers-2.csv          -o csv-data/numbers-2.csv
    curl http://s3.amazonaws.com/presto-example/v2/orders-1.csv           -o csv-data/orders-1.csv
    curl http://s3.amazonaws.com/presto-example/v2/orders-2.csv           -o csv-data/orders-2.csv
    curl http://s3.amazonaws.com/presto-example/v2/lineitem-1.csv         -o csv-data/lineitem-1.csv
    curl http://s3.amazonaws.com/presto-example/v2/lineitem-2.csv         -o csv-data/lineitem-2.csv
    ;;
  presto-start-coordinator)
    _prestoConfigureCoordinator
    $PRESTO_DIR/bin/launcher start
    _holdContainerOpen
    ;;
  presto-start-worker)
    _prestoConfigureWorker
    $PRESTO_DIR/bin/launcher start
    _holdContainerOpen
    ;;
  build-all)
    for DOCKER_TAG_NAME in "${ALL_BUILD_LIST[@]}"; do
      _buildContainer $DOCKER_TAG_NAME
    done
    #docker system prune --all --force
    docker images
    ;;
  push-all)
    for TDIR in "${ALL_BUILD_LIST[@]}"; do
      _pushContainer $TDIR
    done
    #docker system prune --all --force
    docker images
    ;;
  create-all)
    for TDIR in "${ALL_RUN_LIST[@]}"; do
      echo "Creating $TDIR"
      $KUBECTL_CMD create -f $TDIR/main.yaml
    done
    ;;
  apply-all)
    for TDIR in "${ALL_RUN_LIST[@]}"; do
      echo "Applying $TDIR"
      $KUBECTL_CMD apply -f $TDIR/main.yaml
    done
    ;;
  delete-all)
    for TDIR in "${ALL_RUN_LIST[@]}"; do
      echo "Deleting $TDIR"
      $KUBECTL_CMD delete --filename $TDIR/main.yaml
    done
    ;;
  replace)
    BUILD_DIR=$2
    $KUBECTL_CMD replace --force --filename $BUILD_DIR/main.yaml
    ;;
  replace-all)
    for TDIR in "${ALL_RUN_LIST[@]}"; do
      echo "Replacing $TDIR"
      $KUBECTL_CMD replace --force --filename $TDIR/main.yaml
    done
    ;;
  stop)
    $KUBECTL_CMD -n default delete pod,svc --all
    ;;
  logdns)
    $KUBECTL_CMD logs --follow -n kube-system --selector 'k8s-app=kube-dns'
    ;;
  restart-mk8)
    sudo snap disable microk8s
    sudo snap enable microk8s
    ;;
  services)
    $KUBECTL_CMD get services --all-namespaces -o wide
    ;;
  ns)
    $KUBECTL_CMD get all --all-namespaces
    ;;
  build) # Expects Argument APP_ID
    DOCKER_TAG_NAME=${2:-"DOCKER TAG MISSING"}
    _buildContainer $DOCKER_TAG_NAME
    ;;
  push)
    BUILD_DIR=$2
    _pushContainer $BUILD_DIR
    ;;
  logs)
    kubectl logs -n airflow -f worker-57f869d7c6-2cjfx
    ;;
  docker-del-all)
    docker rmi $(docker images -q) -f
    docker system prune --all --force
    ;;
  port-forward)
    K8S_RESOURCE="deployment/webserver"
    NAME_SPACE="airflow"
    IP_ADDR=`hostname -i`
    SRC_PORT="8888"
    DST_PORT="8080"
    # netstat -ltnp
    echo "Forwarding $SRC_PORT to $DST_PORT on $IP_ADDR for $NAME_SPACE - $K8S_RESOURCE"
    pkill -f 'kubectl port-forward'
    nohup microk8s.kubectl port-forward --address $IP_ADDR --namespace $NAME_SPACE $K8S_RESOURCE $SRC_PORT:$DST_PORT &
    ps -ef | grep port-forward
    ;;
  k8s-install)
    sudo snap install microk8s --classic
    microk8s start
    microk8s.enable dns
    microk8s.enable metrics-server
    #microk8s.enable dashboard
    ;;
  get-metrics)
    microk8s.enable get --raw /apis/metrics.k8s.io/v1beta1/pods
    ;;
  dashboard-token)
    token=$(microk8s.kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
    microk8s.kubectl -n kube-system describe secret $token
    # kc proxy
    # ssh -N -L 8888:localhost:8001 r-apps
    # http://localhost:8888/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login
    ;;
  python-configure)
    pip3 install mysql-connector-python
    pip3 install presto-python-client
    ;;
  help)
    echo "create, stop, restart, list, delete"
    ;;
  hold)
    _holdContainerOpen
    ;;
  *)
    echo "Not Found " "$@"
    ;;
esac
