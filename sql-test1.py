import mysql.connector
import prestodb

# Use the Sakila database from https://dev.mysql.com/doc/index-other.html

def printRows(rows):
    for row in rows:
        print( row )

numberOfRows = 5

# MySQL
print( "MySql")
ms1 = mysql.connector.connect(host='localhost', port=3306, database='sakila', user='root', password='welcome1')
c1 = ms1.cursor(prepared=True)
sqlStr1 = """select * from sakila.city limit %s ;"""
c1.execute( sqlStr1, (numberOfRows,) )
rows = c1.fetchall()
printRows( rows )

# prestodb
print( "Presto")
p1 = prestodb.dbapi.connect(host='localhost', port=8080, user='admin', catalog='mysql0', schema='sakila')
c1 = p1.cursor()
c1.execute("select * from mysql0.sakila.city limit {nRows}".format(nRows = numberOfRows))
rows = c1.fetchall()
printRows( rows )
