FROM prestosql/presto:latest

# Add Catalogs
COPY --chown=presto:presto presto-config/etc/catalog/mysql0.properties                /usr/lib/presto/default/etc/catalog/
COPY --chown=presto:presto presto-config/etc/catalog/example-file-iris.properties     /usr/lib/presto/default/etc/catalog/
COPY --chown=presto:presto presto-config/etc/catalog/example-file.properties          /usr/lib/presto/default/etc/catalog/
COPY --chown=presto:presto presto-config/etc/catalog/example-http.properties          /usr/lib/presto/default/etc/catalog/

# Add CSV Data
COPY --chown=presto:presto csv-data /usr/lib/presto/default/csv-data

WORKDIR /usr/lib/presto/default/
EXPOSE 8080
