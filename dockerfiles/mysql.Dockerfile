#
FROM mysql:8.0

ENV MYSQL_ROOT_PASSWORD=welcome1
ENV CONTAINER_CONFIG_DIR=/container-config

RUN apt-get update && apt-get install -y procps

COPY ctl.sh                                                  ${CONTAINER_CONFIG_DIR}/
ADD mysql-config                                             ${CONTAINER_CONFIG_DIR}/mysql-config
ADD downloads/lahman-mysql-dump.sql.gz                       ${CONTAINER_CONFIG_DIR}/baseball/lahman-mysql-dump.sql.gz
ADD downloads/presto-the-definitive-guide/iris-data-set      ${CONTAINER_CONFIG_DIR}/iris-data-set
ADD downloads/presto-the-definitive-guide/flight-data-set    ${CONTAINER_CONFIG_DIR}/flight-data-set
ADD downloads/LearningSQLExample.sql                         ${CONTAINER_CONFIG_DIR}/LearningSQLExample.sql
ADD downloads/sakila-db.tar.gz                               ${CONTAINER_CONFIG_DIR}/

# Entrypoint
ENTRYPOINT $CONTAINER_CONFIG_DIR/ctl.sh mysql-start-db
#CMD hold

#CMD [ "mysql-start-db" ]
#CMD [ "sleep", "9999" ]
