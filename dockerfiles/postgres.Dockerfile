#
FROM postgres:12.3

ENV POSTGRES_PASSWORD=welcome1
ENV CONTAINER_CONFIG_DIR=/container-config

RUN apt-get update && apt-get install -y procps

COPY ctl.sh                                                  ${CONTAINER_CONFIG_DIR}/
ADD postgres-config                                          ${CONTAINER_CONFIG_DIR}/postgres-config
ADD downloads/presto-the-definitive-guide/flight-data-set    ${CONTAINER_CONFIG_DIR}/flight-data-set

# Override ENTRYPOINTs

#USER postgres

# Entrypoint
ENTRYPOINT $CONTAINER_CONFIG_DIR/ctl.sh
CMD [ "postgres-start-db" ]
