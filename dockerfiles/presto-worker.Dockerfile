FROM prestosql/presto:latest

COPY --chown=presto:presto presto-config/etc/worker.config.properties /usr/lib/presto/default/etc/config.properties

EXPOSE 8080
