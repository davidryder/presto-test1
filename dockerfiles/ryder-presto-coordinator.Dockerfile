FROM ubuntu-base:latest

ARG USER
ARG HOME_DIR
ARG PRETO_TMP_DIR
ARG PRESTO_DIR
ARG PRESTO_DATA_DIR
ARG PRESTO_CLI_ARCHIVE

COPY --from=presto-base --chown=${USER}:${USER}   ${PRESTO_TMP_DIR}    ${HOME_DIR}

COPY --chown=${USER}:${USER}  downloads/${PRESTO_CLI_ARCHIVE} ${HOME_DIR}
RUN chmod +x ${HOME_DIR}/${PRESTO_CLI_ARCHIVE}

COPY --chown=${USER}:${USER}  presto-config ${PRESTO_DIR}
RUN mkdir -p ${PRESTO_DATA_DIR}

COPY envvars.sh ${HOME_DIR}
COPY ctl.sh ${HOME_DIR}

EXPOSE 8080

ENTRYPOINT [ "./ctl.sh", "presto-start-coordinator" ]
