FROM starburstdata/presto:latest

COPY --chown=presto:presto presto-config/etc/starburstdata.coordinator.config.properties /usr/lib/presto/etc/config.properties
COPY --chown=presto:presto presto-config/etc/catalog/mysql0.properties /usr/lib/presto/etc/catalog/

EXPOSE 8080
