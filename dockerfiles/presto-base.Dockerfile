FROM scratch as presto-base

ARG PRESTO_VERSION
ARG PRESTO_SERVER_ARCHIVE
ARG PRESTO_DIR
ARG PRESTO_DATA_DIR

# Presto Server Unzipped
ADD downloads/${PRESTO_SERVER_ARCHIVE}                ${PRESTO_TMP_DIR}/
#COPY presto-config                          ${PRESTO_DIR}/
