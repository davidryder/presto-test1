# Ubuntu Base
FROM ubuntu:18.04 as ubuntu-base
#
# Ubuntu Base image
#
ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=linux
ARG USER
ARG HOME_DIR


USER root
RUN apt-get update -yqq   &&  \
    apt-get upgrade -yqq  &&  \
    apt-get install -yqq  build-essential zip vim curl sudo \
    net-tools iputils-ping iproute2 sudo dnsutils \
    uuid-runtime \
    default-jre python python-pip  && \
    apt-get -y clean

# python3 python3-pip default-jdk && \
#    rm -rf /var/lib/apt/lists/*
# Testing: wget dnsutils dnsmasq

RUN adduser --disabled-password --gecos '' ${USER} && \
    adduser ${USER} sudo && \
    echo "${USER} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
USER ${USER}:${USER}
WORKDIR ${HOME_DIR}

COPY --chown=${USER}:${USER} ctl.sh   ${HOME_DIR}/

# Start and hold open
ENTRYPOINT [ "./ctl.sh", "hold" ]
