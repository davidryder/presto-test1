# presto-test1

This repository provides instructions and automation scripts to build an environment running Preso, and databases including MySql and PrestoSQL. 

The MySQL database is loaded with data tables from the iris data set and [Sean Lahman's Baseball DB](http://www.seanlahman.com/). The PostpresSQL database is loaded with the airlines database. Both the iris database and the airlines databases are sourced from [Presto, The Definitive Guide](https://github.com/prestosql/presto-the-definitive-guide)

Clone this repository using the command:

    git clone git@gitlab.com:davidryder/presto-test1.git

To build the docker containers a set of downloads are required. Use the following command to downloads this set of archives:

    ./ctl.sh configure

This will create the downloads directory with the Preso binaries (Server and Cli) and the iris and baseball databases.

To build the containers for the Databases, Presto Coordinator and Presto worker use the command:

    ./ctl.sh build-all

The above command may take around 5 minutes to completes. It builds base images for Ubuntu and Presto, and then builds the Coordiantor and Worker and MySql Database images. Validate these images have been created using the Docker command:

    docker images

You should see the following:
```
REPOSITORY           TAG                 IMAGE ID            CREATED                  SIZE
postgres             latest              d518c92f2548        Less than a second ago   335MB
mysql                latest              7d8deb062138        19 seconds ago           576MB
presto-worker        latest              351a3ccc1782        43 seconds ago           1.33GB
presto-coordinator   latest              4425d7910505        51 seconds ago           1.36GB
presto-base          latest              438f1d412437        About a minute ago       499MB
ubuntu-base          latest              02359409b484        About a minute ago       835MB
postgres             12.3                b03968f50f0e        6 days ago               313MB
mysql                8.0                 0d64f46acfd1        6 days ago               544MB
ubuntu               18.04               2eb2d388e1a2        2 weeks ago              64.2MB
```

This set of containers can be started using the command:

    ./ctl.sh start-all

Validate the containers have started using the comnand:

    docker ps

You should see following set of running containers:
```CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                    NAMES
b6f9861f8097        presto-worker        "./ctl.sh presto-sta…"   54 minutes ago      Up 54 minutes       8080/tcp                 presto-worker1
63d7356b12c4        presto-worker        "./ctl.sh presto-sta…"   54 minutes ago      Up 54 minutes       8080/tcp                 presto-worker0
83f0cad4f86f        presto-coordinator   "./ctl.sh presto-sta…"   54 minutes ago      Up 54 minutes       0.0.0.0:8080->8080/tcp   presto-coordinator0
cf2b67afb7bc        mysql                "./ctl.sh mysql-star…"   54 minutes ago      Up 54 minutes       3306/tcp, 33060/tcp      mysql1
350f449f156b        mysql                "./ctl.sh mysql-star…"   54 minutes ago      Up 54 minutes       3306/tcp, 33060/tcp      mysql0
```

Notice that one Preso coordinator and two workers are running, along with two mysql databases. The Preso coordinator has been configued with the catalogs mysql0, mysql0 and tpch 

Once these containers are running use the Preso CLI to connect to the Coordinator node:

    ./presto

Use the SHOW CATALOGS command to see what databases are available:

    presto> SHOW CATALOGS
    Catalog 
    ---------
    mysql0  
    mysql1  
    system  
    tpch    
    (4 rows)

    Query 20200810_210355_00012_i4762, FINISHED, 2 nodes
    Splits: 36 total, 36 done (100.00%)
    0.39 [0 rows, 0B] [0 rows/s, 0B/s]

Review the tables in catalog mysql0 and the schema baseball:

    presto> show tables from mysql0.baseball;
           Table        
    ---------------------
    allstarfull         
    appearances         
    awardsmanagers      
    awardsplayers       
    awardssharemanagers 
    awardsshareplayers  
    batting             
    battingpost         
    collegeplaying      
    divisions           
    fielding     

Run a query to see the top 5 baseball teams who have won the most Super Bowls:

    presto> select  distinct a.teamId, count(*)  from mysql0.baseball.teams a where a.wswin = 'Y' group by a.teamId order by count(*) desc limit 5;
    teamId | _col1 
        --------+-------
        NYA    |    27 
        SLN    |    11 
        BOS    |     9 
        NY1    |     7 
        PIT    |     5 
        LAN    |     5 
    
    