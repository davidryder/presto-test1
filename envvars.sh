
export USER="ddr"
export HOME_DIR="/home/$USER"
export PRESTO_VERSION="339"
export PRESTO_SERVER_ARCHIVE="presto-server-${PRESTO_VERSION}.tar.gz"
export PRESTO_CLI_ARCHIVE="presto-cli-${PRESTO_VERSION}-executable.jar"
export PRESTO_TMP_DIR="/presto"
export PRESTO_DIR="$HOME_DIR/presto-server-${PRESTO_VERSION}"
export PRESTO_DATA_DIR="$PRESTO_DIR/data"

export DOCKERFILES_DIR="dockerfiles"
export DOCKER_NETWORK_NAME="testnet1"
